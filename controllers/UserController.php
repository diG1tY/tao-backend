<?php
/**
 * Created by Iliyan Petrov.
 * Date: 1/12/2019
 * Time: 2:13 PM
 */

namespace app\controllers;

use yii\base\Module;
use yii\base\NotSupportedException;
use yii\rest\ActiveController;
use app\services\UserService;

class UserController extends ActiveController
{
    /* This is needed by the Yii framework, but since we are using data files,
        we actually don't need models/entities
     */
    public $modelClass = 'app\models\User';
    public $userService;

    /**
     * UserController constructor.
     * @param $id
     * @param Module $module
     * @param array $config
     */
    public function __construct($id, Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->userService = new UserService();
    }

    /**
     * Check if the provided format is supported
     *
     * @param String $format
     * @throws NotSupportedException
     */
    private function checkForFormats(String $format)
    {
        if (!in_array($format, $this->userService::ALLOWED_FORMATS)) {
            throw new NotSupportedException();
        }
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::actions();
        // We remove the default index and view actions
        // by default they use DB layer, which at the moment we don't need
        unset($actions['index'], $actions['view']);

        return $actions;
    }

    /**
     * @param string $format
     * @param string $name
     * @param int $limit
     * @param int $offset
     * @return mixed
     * @throws NotSupportedException
     */
    public function actionIndex($format = 'json', $name = '', $limit = 20, $offset = 0)
    {
        $this->checkForFormats($format);
        $filters = [
            'name' => $name,
            'limit' => $limit,
            'offset' => $offset,
        ];

        return $this->userService->getAllUsers($format, $filters);
    }

    /**
     * @param $id
     * @param string $format
     * @return array
     * @throws NotSupportedException
     */
    public function actionView($id, $format = 'json')
    {
        $this->checkForFormats($format);

        return $this->userService->getUserById((int)$id, $format);
    }
}