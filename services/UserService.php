<?php
/**
 * Created by Iliyan Petrov.
 * Date: 1/12/2019
 * Time: 3:26 PM
 */

namespace app\services;

use app\repositories\UserRepository;

class UserService
{
    private $userRepository;
    private $filters = [];

    const ALLOWED_FORMATS = ['csv', 'json'];
    const SOURCE = 'file';

    /**
     * UserService constructor.
     */
    public function __construct()
    {
        $this->userRepository = new UserRepository();;
    }

    /**
     * @param string $format
     * @return array
     */
    private function getData(string $format)
    {
        if (self::SOURCE === 'file') {
            return $this->getDataFromFile($format);
        } else {
            // Future Implementation(eg DB)
        }
    }

    /**
     * Get the data from the file
     *
     * @param $format
     * @return array
     */
    private function getDataFromFile(string $format)
    {
        $methodName = 'parseUsersFrom' . ucfirst($format);

        $users = $this->userRepository->$methodName();

        return $this->applyFilters($users);
    }

    /**
     * @param array $users
     * @return array
     */
    private function applyFilters(array $users): array
    {
        $foundUsers = [];

        // Search for the name
        if ($this->filters['name'] != '') {
            foreach ($users as $user) {

                if ($this->filters['name'] == $user['firstname'] || $this->filters['name'] == $user['lastname']) {
                    $foundUsers[] = $user;
                }
            }
        } else {
            $foundUsers = $users;
        }

        // Then apply offset & limit
        $foundUsers = array_slice($foundUsers, $this->filters['offset'], $this->filters['limit'], true);

        return $foundUsers;
    }

    /**
     * @param string $format
     * @param array $filters
     * @return array
     */
    public function getAllUsers(string $format, array $filters = [])
    {
        $this->filters = $filters;

        return $this->getData($format);
    }

    /**
     * @param int $id
     * @param string $format
     * @return array|mixed
     */
    public function getUserById(int $id, string $format)
    {
        return array_key_exists($id, $this->getData($format)) ? $this->getData($format)[$id] : [];
    }
}