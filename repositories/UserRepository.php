<?php
/**
 * Created by Iliyan Petrov.
 * Date: 1/12/2019
 * Time: 3:27 PM
 */

namespace app\repositories;

class UserRepository
{
    /**
     * From PHP docs.
     * @param string $filename
     * @param string $delimiter
     * @return array|bool
     */
    private function convertCsvFileToArray(string $filename = '', string $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename)) {
            return false;
        }

        $header = null;
        $data = [];
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header) {
                    $header = $row;
                } else {
                    $data[] = array_combine($header, $row);
                }
            }
            fclose($handle);
        }

        return $data;
    }

    /**
     * @return array
     */
    public function parseUsersFromJson(): array
    {
        $string = file_get_contents(\Yii::getAlias('@app' . '/data/testtakers.json'));
        return json_decode($string, true);
    }

    /**
     * @return array
     */
    public function parseUsersFromCsv(): array
    {
        return $this->convertCsvFileToArray(\Yii::getAlias('@app' . '/data/testtakers.csv'));
    }
}